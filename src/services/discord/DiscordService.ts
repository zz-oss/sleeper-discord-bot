import { CiaoEmbed } from "../../data/CiaoEmbed";
import { commandoClient } from '../../index'
import { Channel } from 'discord.js';

export class DiscordService {
    async makeCiaoEmbedAsync(title: string): Promise<CiaoEmbed> {
        const embed = new CiaoEmbed(title)

        return embed;
    }
    
    async getChannelNameAsync(channelId: string): Promise<Channel> {
        const channel = await commandoClient.channels.fetch(channelId)

        return channel
    }
}