import { SleeperClient } from '../../../sleeper-api-ts-wrapper/src/Clients/SleeperClient'

export class BaseSleeperService {
    sleeper: SleeperClient
    constructor() {
        this.sleeper = new SleeperClient()
    }
}