const { Command } = require('discord.js-commando')
const { SleeperService } = require('../../services/SleeperService')
const { DiscordService } = require('../../services/discord/DiscordService')
const { DiscordLeagueService } = require('../../services/sql/DiscordLeagueService')

module.exports = class TradesCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'trades',
			description: 'returns trade info for a specific week',
            examples: ['^trades <week>',],
            group: 'league',
            memberName: 'trades',
            args: [
                {
                    key: 'week',
                    prompt: 'Enter the week you\'d like me to fetch the trade data for',
                    type: 'integer'
                }
            ]
		})

		this.sleeperService = new SleeperService()
		this.discordService = new DiscordService()
		this.discordLeagueService = new DiscordLeagueService()
	}

	async run(msg, { week }) {
        const result = await this.discordLeagueService.getLeagueIdAsync(msg)
		let message

		if (result.success) {
			const leagueId = result.data
			const transactions = await this.sleeperService.league.getTransactionsAsync(leagueId, week)

			message = await this.discordService.makeCiaoEmbedAsync('Trade Info')
            transactions.forEach(async t => {
                if (t.status === 'complete' && t.type === 'waiver') {
                    let playerId, rosterId, player, user, roster

                    if (t.adds !== null) {
                        playerId = Object.keys(t.adds)[0]
                        rosterId = t.adds[playerId]
                        player = await this.sleeperService.player.getPlayerInfoAsync(playerId)
                        roster = await this.sleeperService.league.getRosterByIdAsync(leagueId, rosterId)
                        user = await this.sleeperService.user.getUserInfoByIdAsync(roster.owner_id)
                        console.log(`TradesCommand: ${player}`)

                        message.addField(`${user.display_name} added: `, player.full_name)
                    }
                    if (t.drops !== null) {
                        playerId = Object.keys(t.drops)[0]
                        rosterId = t.drops[playerId]
                        player = await this.sleeperService.player.getPlayerInfoAsync(playerId)
                        roster = await this.sleeperService.league.getRosterByIdAsync(leagueId, rosterId)
                        user = await this.sleeperService.user.getUserInfoByIdAsync(roster.owner_id)

                        message.addField(`${user.display_name} dropped: `, player.full_name)
                    }
                }
            })
		} else {
			message = result.data
		}

		return msg.channel.send(message)
	}
}
