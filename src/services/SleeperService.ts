import { Player } from "../../../sleeper-api-ts-wrapper/src/models/players/Player";
import { LeagueService } from "./sleeper/LeagueService";
import { PlayerService } from "./sleeper/PlayerService";
import { UserService } from "./sleeper/UserService";

export class SleeperService {
    league: LeagueService
    player: PlayerService
    user: UserService
    constructor() {
        this.league = new LeagueService()
        this.player = new PlayerService()
        this.user = new UserService()
    }
}