import { BaseSleeperService } from "../BaseSleeperService";
import { LeagueViewModel } from "../../../../sleeper-api-ts-wrapper/src/models/leagues/LeagueViewModel";
import { DraftViewModel } from "../../../../sleeper-api-ts-wrapper/src/models/drafts/DraftViewModel";
import { LeagueReport } from "../../../../sleeper-api-ts-wrapper/src/models/leagues/report/LeagueReport";
import { LeagueTransactionsViewModel } from "../../../../sleeper-api-ts-wrapper/src/models/leagues/LeagueTransactionsViewModel";
import { LeagueRosterViewModel } from "../../../../sleeper-api-ts-wrapper/src/models/leagues/LeagueRosterViewModel";

export class LeagueService extends BaseSleeperService {
    async getLeagueAsync(leagueId: string): Promise<LeagueViewModel> {
        try {
            const league = await this.sleeper.league.getLeagueAsync(leagueId)

            return league;
        } catch (e) {
            console.error
        }
    }

    async getDraftsAsync(leagueId: string): Promise<DraftViewModel[]> {
        try {
            const drafts = await this.sleeper.league.getDraftsAsync(leagueId)


            return drafts;
        } catch (e) {
            console.error
        }
    }

    async getRostersAsync(leagueId: string): Promise<LeagueRosterViewModel[]> {
        try {
            const rosters = await this.sleeper.league.getRostersAsync(leagueId)

            return rosters
        } catch (e) {
            console.error
        }
    }

    async getRosterByIdAsync(leagueId: string, rosterId: number): Promise<LeagueRosterViewModel> {
        try {
            const roster = await this.sleeper.league.getRosterByIdAsync(leagueId, rosterId)

            return roster
        } catch (e) {
            console.error
        }
    }

    async getReportAsync(leagueId: string): Promise<LeagueReport> {
        try {
            const report = await this.sleeper.league.getLeagueReport(leagueId);

            return report
        } catch (e) {
            console.error
        }
    }

    async getTransactionsAsync(leagueId: string, week: number): Promise<LeagueTransactionsViewModel[]> {
        try {
            const transactions = await this.sleeper.league.getTransactionsAsync(leagueId, week)

            return transactions
        } catch (e) {
            console.error
        }
    }
}