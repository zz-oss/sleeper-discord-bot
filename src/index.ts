import { CommandoClient } from "discord.js-commando"

const Commando = require('discord.js-commando')
const Path = require('path')
const Sequelize = require('sequelize')
const PREFIX = '^'
require('dotenv').config()

const sequelize = new Sequelize(process.env.DATABASE, process.env.DBUSER, process.env.DBPASSWORD, {
	host: process.env.DBHOST,
	dialect: 'sqlite',
	logging: false,
	storage: 'database.sqlite'
})

export const players = sequelize.define('Players', {
	position: Sequelize.STRING,
	search_full_name
})

export const discordLeagues = sequelize.define('DiscordLeague', {
	discordId: {
		type: Sequelize.STRING,
		unique: true,
	},
	leagueId: Sequelize.STRING,
	channelId: Sequelize.STRING
});

export const commandoClient = new Commando.Client({
	commandPrefix: PREFIX,
	owner: process.env.USER_ID
})

commandoClient.registry
	.registerDefaultTypes()
	.registerGroups([
		['league', 'Commands for accessing the leagues endpoint'],
		['general', 'Commands for setting up the bot']
	])
	.registerDefaultGroups()
	.registerDefaultCommands()
	.registerCommandsIn(Path.join(__dirname, 'commands'))

commandoClient.once('ready', async () => {
	console.log('Sleeper bot ready to go')
	discordLeagues.sync()
})

commandoClient.login(process.env.BOT_TOKEN)