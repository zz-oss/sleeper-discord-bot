import { CommandoMessage } from 'discord.js-commando'
import { DiscordLeaguesResult } from '../../data/DiscordLeaguesResult'
import { discordLeagues } from '../../index'
import { BaseSleeperService } from '../BaseSleeperService'

export class DiscordLeagueService extends BaseSleeperService {
    constructor() {
        super()
    }

    async registerDiscordLeagueAsync(discordId: string, leagueId: string, channelId: string): Promise<DiscordLeaguesResult> {
        const result = new DiscordLeaguesResult()
        result.success = false
        try {
            const discordLeague = await discordLeagues.create({
                discordId: discordId,
                leagueId: leagueId,
                channelId: channelId
            })

            if (discordLeague.discordId !== undefined &&
                discordLeague.leagueId !== undefined &&
                discordLeague.channelId !== undefined) {
                result.success = true
                result.data = discordLeague
            }

            return result
        } catch {
            console.error
            return result
        }
    }

    async updateDiscordLeagueAsync(discordId: string, leagueId: string, channelId: string): Promise<DiscordLeaguesResult> {
        const result = new DiscordLeaguesResult()
        result.success = false
        try {
            const affectedRows = await discordLeagues.update({ leagueId: leagueId, channelId: channelId }, { where: { discordId: discordId } })
            if (affectedRows > 0) {
                result.success = true
            }

            return result
        } catch (e) {
            console.error
            return result
        }
    }

    async getDiscordLeagueByDiscordIdAsync(discordId: string): Promise<DiscordLeaguesResult> {
        const result = new DiscordLeaguesResult()
        result.success = false
        try {
            const discordLeague = await discordLeagues.findOne({ where: { discordId: discordId } })
            if (discordLeague !== null) {
                result.success = true
                result.data = discordLeague
            }

            return result
        } catch (e) {
            console.error
            return result
        }
    }
    
    async getLeagueIdAsync(message: CommandoMessage): Promise<DiscordLeaguesResult> {
        const result = new DiscordLeaguesResult()
        result.success = false
        try {
            const response = await this.getDiscordLeagueByDiscordIdAsync(message.author.id)
            if (response.success) {
                result.success = true
                result.data = response.data.leagueId
            } else {
                result.data = 'You have to register your account first with `^register`'
            }

            return result
        } catch (e) {
            console.error
            return result
        }
    }

    async registerUpdateDiscordLeagueAsync(discordId: string, leagueId: string, channelId: string): Promise<DiscordLeaguesResult> {
        let result = new DiscordLeaguesResult()
        result.success = false
        try {
            const response = await this.getDiscordLeagueByDiscordIdAsync(discordId)

            if (response.success) {
                result = await this.updateDiscordLeagueAsync(discordId, leagueId, channelId)
            } else {
                result = await this.registerDiscordLeagueAsync(discordId, leagueId, channelId)
            }

            return result
        } catch (e) {
            console.error
            return result 
        }
    }
}