const { Command } = require('discord.js-commando')
const { SleeperService } = require('../../services/SleeperService')
const { DiscordService } = require('../../services/discord/DiscordService')
const { DiscordLeagueService } = require('../../services/sql/DiscordLeagueService')

module.exports = class ReportCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'report',
			description: 'returns the latest league report',
            examples: ['^report',],
            group: 'league',
            memberName: 'report',
		})

		this.sleeperService = new SleeperService()
        this.discordService = new DiscordService()
        this.discordLeagueService = new DiscordLeagueService()
	}

	async run(msg) {
        const result = await this.discordLeagueService.getLeagueIdAsync(msg)
        let message

        if (result.success) {
            const leagueId = result.data
            const report = await this.sleeperService.league.getReportAsync(leagueId)

            message = await this.discordService.makeCiaoEmbedAsync(report.week)
        } else {
            message = result.data
        }

		return msg.channel.send(message)
	}
}
