const { Command } = require('discord.js-commando')
const { SleeperService } = require('../../services/SleeperService')
const { DiscordService } = require('../../services/discord/DiscordService')
const { DiscordLeagueService } = require('../../services/sql/DiscordLeagueService')
const { CiaoEmbed } = require('../../data/CiaoEmbed')

module.exports = class DraftsCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'draft',
            description: 'returns draft info',
            examples: ['^draft',],
            group: 'league',
            memberName: 'draft',
        })

        this.sleeperService = new SleeperService()
        this.discordService = new DiscordService()
        this.discordLeagueService = new DiscordLeagueService()
    }

    async run(msg) {
        const result = await this.discordLeagueService.getLeagueIdAsync(msg)
        let message

        if (result.success) {
            const leagueId = result.data
            const drafts = await this.sleeperService.league.getDraftsAsync(leagueId)

            message = await this.discordService.makeCiaoEmbedAsync(drafts[0].status)
        } else {
            message = result.data
        }

        return msg.channel.send(message)
    }
}
