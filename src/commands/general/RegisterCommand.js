const { Command, CommandoMessage } = require('discord.js-commando')
const { SleeperService } = require('../../services/SleeperService')
const { DiscordLeagueService } = require('../../services/sql/DiscordLeagueService')
const { DiscordService } = require('../../services/discord/DiscordService')

module.exports = class RegisterCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'register',
            description: 'Ties your discord account to a League and tells the bot which channel to post in',
            examples: ['^register <leagueId> <channelId>',],
            group: 'general',
            memberName: 'register',
            args: [
                {
                    key: 'leagueId',
                    prompt: 'Enter your Fantasy LCS League ID, to learn where to find this do ^registerhelp',
                    type: 'string'
                },
                {
                    key: 'channelId',
                    prompt: 'Enter the Channel ID that you want me to post in',
                    type: 'string'
                }
            ]
        })

        this.sleeperService = new SleeperService()
        this.discordLeaguesService = new DiscordLeagueService()
        this.discordService = new DiscordService()
    }

    async run(msg, { leagueId, channelId }) {
        const result = await this.discordLeaguesService.registerUpdateDiscordLeagueAsync(msg.author.id, leagueId, channelId)
        let message

        if (result.success) {
            const league = await this.sleeperService.league.getLeagueAsync(leagueId)

            message = `Your account has been tied to the league: "${league.name}" and I will post about it in <#${channelId}>`
        } else {
            message = `There was an error registering your account`
        }

        return msg.channel.send(message)
    }
}
