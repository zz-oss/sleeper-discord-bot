import { Player } from "../../../../sleeper-api-ts-wrapper/src/models/players/Player";
import { BaseSleeperService } from "../BaseSleeperService";

export class PlayerService extends BaseSleeperService {
    constructor() {
        super()
    }

    async getPlayerInfoAsync(playerId: number): Promise<Player> {
        try {
            const player = await this.sleeper.players.getPlayerByIdAsync(playerId)
            console.log(`PlayerService: ${player}`)

            return player
        } catch (e) {
            console.error
        }
    }
}