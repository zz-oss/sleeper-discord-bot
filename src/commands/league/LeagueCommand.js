const { Command } = require('discord.js-commando')
const { SleeperService } = require('../../services/SleeperService')
const { DiscordService } = require('../../services/discord/DiscordService')
const { DiscordLeagueService } = require('../../services/sql/DiscordLeagueService')

module.exports = class LeagueCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'league',
			description: 'returns league info',
            examples: ['^league',],
            group: 'league',
            memberName: 'league',
		})

		this.sleeperService = new SleeperService()
		this.discordService = new DiscordService()
		this.discordLeagueService = new DiscordLeagueService()
	}

	async run(msg) {
        const result = await this.discordLeagueService.getLeagueIdAsync(msg)
		let message

		if (result.success) {
			const leagueId = result.data
			const league = await this.sleeperService.league.getLeagueAsync(leagueId)

			message = await this.discordService.makeCiaoEmbedAsync(league.name)
		} else {
			message = result.data
		}

		return msg.channel.send(message)
	}
}
