import { UserViewModel } from "../../../../sleeper-api-ts-wrapper/src/models/users/UserViewModel";
import { BaseSleeperService } from "../BaseSleeperService";

export class UserService extends BaseSleeperService {
    constructor() {
        super()
    }

    async getUserInfoByIdAsync(userId: string): Promise<UserViewModel> {
        try {
            const user = await this.sleeper.user.getAsync(userId)

            return user
        } catch (e) {
            console.error
        }
    }
}