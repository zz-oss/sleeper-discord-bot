import { MessageEmbed } from 'discord.js'

export class CiaoEmbed extends MessageEmbed {
    constructor(title: string,) {
        super()

        this.setColor('#0099ff')
        this.setTitle(title)
        this.setURL(`https://sleeper.app/leagues/656648984051425280`)
        this.setTimestamp()
        this.setFooter('Sleeper Fantasy LCS Bot by Ciao');
    }
}